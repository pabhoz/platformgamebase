var escena,camaras = {}, models ={},renderer, camaraActiva = null, bounds = {}, ultiTiempo;
var appW = window.innerWidth, appH = window.innerHeight;
var TECLA = { ARRIBA:false, ABAJO:false, IZQUIERDA:false, DERECHA:false, A:false,W:false,S:false,D:false};
var prevTime = performance.now();


var collidableMeshList = [];
var level = 1;
//fisicas
var vy = 0, vx = 0, gravity = 0.3;
var jumping = false, inAir = true, falling = false;

var cargador = {
	loadState: false,
	objsToLoad: 0,
	objsLoaded: 0,
	sceneIsReady: false,
	objReady: function(){
		this.objsToLoad--;
		this.objsLoaded++;
		var total = this.objsToLoad+this.objsLoaded;
		var porcentaje = (this.objsLoaded/total)*100;
		$("#loadingBar").html(porcentaje+"%");
		if(this.objsToLoad == 0){
			this.loadState = true;
			$('#loadingBar').fadeToggle(4000);
			$("#loadingBar").html("A jugar viejo!");
		}
	},
	addObj: function(){
		this.objsToLoad++;
	}
};

function webGLStart(){
	iniciarEscena();
	$( window ).resize(onWindowResize);
	ultiTiempo = Date.now();
	document.onkeydown = teclaPulsada;
	document.onkeyup = teclaSoltada;
	animarEscena();
}

function iniciarEscena(){
	renderer = new THREE.WebGLRenderer();
	renderer.setClearColor(0x6a6a6a, 1);
	renderer.setSize(appW, appH);
	renderer.shadowMapEnabled = true;
	document.body.appendChild(renderer.domElement);


	var camara = new THREE.PerspectiveCamera(45, appW / appH, 1, 10000);
	camara.position.set(0,500,1000);
	camara.lookAt(new THREE.Vector3(0,0,0));

	camaras.interactive = camara;
	camaraActiva = camaras.interactive;

	escena = new THREE.Scene();

	//Iniciar controles de la camara
	controlCamara = new THREE.OrbitControls( camaras.interactive , renderer.domElement );

	//estadisticas
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.left = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild(stats.domElement);

	//Luz hemisferica
	var light = new THREE.HemisphereLight( 0xeeeeff, 0x777788, 0.75 );
	light.position.set( 0.5, 1, 0.75 );
	escena.add( light );

	//Luz AMbiente
	var lambiente = new THREE.AmbientLight(0x3d3c3c);
	escena.add(lambiente);

	//Luz Spotlight
	var spotLight = new THREE.SpotLight( 0xffffff,1,4000,30,0.1 );
	spotLight.position.set( 0, 1000, 0 );
	spotLight.castShadow = true;
	escena.add( spotLight );

	//Suelo
	var plano = new THREE.Mesh(
		new THREE.BoxGeometry(1500,1,1500),
		new THREE.MeshLambertMaterial( {color:0xbdbcBc,map:THREE.ImageUtils.loadTexture("textures/Floor_Lava512.jpg")})
	);
	plano.position.set(0,-1,0);
	plano.castShadow = false;
	plano.receiveShadow = true;
	
	plano.material.map.wrapS = THREE.RepeatWrapping;
	plano.material.map.wrapT = THREE.MirroredRepeatWrapping;
	plano.material.map.repeat.set(10,10);

	escena.add(plano);
	collidableMeshList.push(plano);

	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/brick-block.png")})
	);
	box.position.set(50,80,0);
	box.castShadow = true;
	box.receiveShadow = true;
	escena.add(box);
	collidableMeshList.push(box);

	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/brick-block.png")})
	);
	box.position.set(100,160,0);
	box.castShadow = true;
	box.receiveShadow = true;
	escena.add(box);
	collidableMeshList.push(box);

	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/brick-block.png")})
	);
	box.position.set(100,160,50);
	box.castShadow = true;
	box.receiveShadow = true;
	escena.add(box);
	collidableMeshList.push(box);

	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/brick-block.png")})
	);
	box.position.set(150,240,50);
	box.castShadow = true;
	box.receiveShadow = true;
	escena.add(box);
	collidableMeshList.push(box);

	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/brick-block.png")})
	);
	box.position.set(150,320,0);
	box.castShadow = true;
	box.receiveShadow = true;
	box.name="powerUp";
	escena.add(box);
	collidableMeshList.push(box);

	/****
	Esta caja responderà como las cajas de coins de mario
	con sonido y movimiento.
	***/
	box = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial( {color:0xffff00,map:THREE.ImageUtils.loadTexture("textures/signo.png")})
	);
	box.position.set(50,80,200);
	box.castShadow = true;
	box.receiveShadow = true;
	escena.add(box);
	collidableMeshList.push(box);

	//Creación de los rayos para el calculo de colisiones
	ray = new THREE.Raycaster();
	ray.ray.direction.set( 0, -1, 0 );
	rayUp = new THREE.Raycaster();
	rayUp.ray.direction.set( 0, 1, 0 );


	//PLAYER
	p1 = new THREE.Mesh(
		new THREE.SphereGeometry(10,10,10),
		new THREE.MeshPhongMaterial({color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/adidas.jpg")})
		);
	p1.castShadow = true;
	p1.receiveShadow = true;
	p1.position.y = 5;
	escena.add(p1);


	//ENEMIGO

	enemy = new THREE.Mesh(
		new THREE.BoxGeometry(50,50,50),
		new THREE.MeshLambertMaterial({color:0xffffff,map:THREE.ImageUtils.loadTexture("textures/steel-box.jpg")})
		);
	enemy.castShadow = true;
	enemy.receiveShadow = true;
	enemy.position.set(150,320,100);
	enemy.name = "muerte";
	escena.add(enemy);
	collidableMeshList.push(enemy);

	}

	function animarEscena(){
		requestAnimationFrame( animarEscena );

		if(!cargador.loadState && cargador.objsToLoad > 0){
		console.log("Obj Loaded : "+cargador.objsLoaded+" / "+(cargador.objsToLoad+cargador.objsLoaded));
		}else{
			if(!cargador.sceneIsReady){
				cargarModelos();
				cargador.sceneIsReady = true;
			}
			renderEscena();
			actualizarEscena();
		}
	}

	function renderEscena(){
		renderer.render( escena, camaraActiva );
	}

	function cargarModelos(){
		for (var i = 0; i < Object.keys(models).length; i++) {
			var modelo = models[Object.keys(models)[i]];
			if("added" in modelo){
				if(!modelo.added && modelo.obj instanceof THREE.Object3D){
					escena.add(modelo.obj);
					modelo.added = true;
				}
			}
		};
	}

	function actualizarEscena(){

		vy+=gravity;
		if (inAir){	
			if (vy>10){
				vy=10;
			}
		}
		
		/*******
			Agregar sonido de salto
		*********/
		if(TECLA.ESPACIO){ jump(p1); }

		if (TECLA.IZQUIERDA) {
			p1.position.x+=vx;	
		}
		if (TECLA.DERECHA) {
			p1.position.x+=vx;
		}

		if (TECLA.ARRIBA) {
			vx = -2;
			p1.position.z+=vx;	
		}
		if (TECLA.ABAJO) {
			vx = 2;
			p1.position.z+=vx;	
		}	

		//Rotación de la esfera
		if ( TECLA.IZQUIERDA){
			vx = -2;
	        p1.rotation.y = 3.14;
		
		}
		if ( TECLA.DERECHA ){
			vx = 2;
	        p1.rotation.y = 0;
		}

		/*******
			ACTIVIDAD 1: Rotar la esfera conforme al movimiento, observe el ejemplo superior
		********/

		if (TECLA.ARRIBA) {
			p1.rotation.y = 3.14/2;
		}
		if (TECLA.ABAJO) {
			p1.rotation.y = (3*3.14)/2;
		}


		//Colisiones					
		var originPoint = p1.position.clone(); //Punto de origen de la colisión
		ray.ray.origin.copy( originPoint ); 
		rayUp.ray.origin.copy( originPoint );

		var intersecciones = rayUp.intersectObjects( collidableMeshList ); //Obtenemos las intersecciones por colisiones en y+ entre los objetos de la lista de colisiones y nuestro personaje.

		if ( intersecciones.length > 0 ) { //Si existen colisiones
			var distance = intersecciones[ 0 ].distance; //distancia entre las intersecciones
			if ( falling == false && distance > 0 && distance <= 6 ) {//Si el jugador no está cayendo y la distancia de la intersección es mayor a 0 y menor o igual a 10 (para que no genere colisiones exageradas se limita a las unidades que en este caso son el tamaño del jugador)
				vy = 0; //Para los que aún no lo notan vy es la velocidad en y que se gana al saltar y se pierde al caer
				falling = true;
			}
		}

		var intersections = ray.intersectObjects( collidableMeshList ); //Obtenemos las intersecciones por colisiones en y- entre los objetos de la lista de colisiones y nuestro personaje.

		if ( intersections.length > 0 ) {
			var distance = intersections[ 0 ].distance;
			if ( distance > 0 && distance > 11 ) {
				p1.position.y -= vy;
				inAir = true;
			}else{

				/**
					Colisión evaluada solo si cae sobre el elemento!
					(colisión para plataforma);
				**/
				/*******
					Agregar sonido de Muerte
				*********/
				if(intersections[ 0 ].object.name == "muerte"){
					p1.position.set(0,5,0);
					p1.material.color = new THREE.Color(0xffffff);
				}
				/*******
					Agregar sonido de powerUp
				*********/
				if(intersections[ 0 ].object.name == "powerUp"){
					p1.material.color = new THREE.Color(0x00ff00);
				}
				//como estoy sobre una plataforma entonces
				//reinicio los estados
				falling = false;
				inAir = false;
				vy=0;
				jumping = false;
			}
		}

		if (inAir == false && distance < 10){ //Corregimos el parkinson del jugador
			p1.position.y+=1;
		}

		/*******
			ACTIVIDAD 2: Mover las plataformas de manera constante entre +/- 50 unidades en Z
		********/




		stats.update();
		controlCamara.update();
	}
